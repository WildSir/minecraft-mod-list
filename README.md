# Minecraft Mod List

## What is this?

This is a project I am working on to improve my Svelte skills. It is basically a website that is used to make a list of your favorite mods, like an Amazon wishlist of the products you want (mods).

## What is the future of this project?

I am using this to relearn some simple skills I forgot using React. For example, I forgot all about CSS after using TailwindCSS for a long time.
I wil probably turn this into a full featured application with features like sharing your mod lists later when I feel like I'm comfortable with Svelte. After I feel like I mastered CSS (Or SCSS) and JavaScript (Or TypeScript) again I probably switch to modern technologies like TailwindCSS.

## To-Do

- [x] Seperate into dev and stable branches
- [ ] Add some data sanitization and stuff
- [ ] Create RegEx for Minecraft Snapshot Versions and Release Candiates
- [ ] More elaborate mod selection
- [x] Install Sass
- [ ] Also validate mod versions selected
- [x] Implement the VersionDatalist component
- [ ] Improve looks using vanilla CSS
- [x] Start doing version numberss
