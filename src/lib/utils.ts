import type { Mod, Version } from "./types/mod.type";


const getVersionsBySlug = async (slug:string) => {
    // response is 200 or 404
    try {
        const res = await fetch(`https://api.modrinth.com/v2/project/${slug}/version`);
        const data = await res.json();
        //using a set to get rid of duplicate versions
        const versions:Set<string> = new Set();
        for (const item of data) {
           for (const gameVersions of item.game_versions) {
                versions.add(gameVersions);
           } 
        }
        return Array.from(versions);
    } catch (err) {
        throw new Error(
            `404: ${slug.toLowerCase()}'s verions not found or no authorization`
        )
    }
}
const getModBySlug = async (slug:string): Promise<Mod> => {
    // server will respond with 404 or 200
    try {
        const res = await fetch(`https://api.modrinth.com/v2/project/${slug}`);
        // if 404, the data will not be JSON, so there will be a error while parsing
        const mod = await res.json();
        // when slug is an empty string, the mod will be undefined (I think)
        if(mod.id === undefined){
            throw new Error()
        }
        return {
            title:mod.title,
            id:mod.id,  
            slug:mod.slug
        }
    }
    catch (err) {
        throw new Error(
            `404: "${slug.toLowerCase()}" not found or no authorization`
        )
    }
}
const findMods = async (query:string) => {
    try {
        // Maybe XSS (i forgor what it's called) attack, I should fix this
        const res = await fetch(`https://api.modrinth.com/v2/search?query=${query}&limit=15`); 
        const data = await res.json();
        // Server only can respond with 400 or 200
        // This never should happen, it's just here for safety
        if (!res.ok)
            throw new Error();
        return data.hits
    } catch(err) {
        throw new Error("Error 400: Invalid Request for query")
    }
}

// Implement this later
//const getVersionById = (id:string):Version => {
//    return {modVersion:"", gameVersion:""}
//}
export {getModBySlug, findMods, getVersionsBySlug}