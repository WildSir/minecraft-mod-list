type Version = {
    modVersion:string,
    gameVersion:string,
};
// add version when necessary, and others as they are needed such as loaders
// ModDatalist returns the Mod type, so make sure to edit that
// Note that it's problably not the only one that uses it, I have to check 
// Check out ListItem.svelte too
type Mod = {
    id:string,
    title:string,
    slug:string,
    //versions:Array<Version>,
};

export type {Mod, Version};